---
title: app爬虫
date: 2020-02-01
lastmod: 2020-01-30
author: Bob Dylan
cover: /img/default4.jpg
categories: ["python"]
tags: ["python"]
# showcase: true
draft: true
grammar_mermaid: true
---

[toc]

# APP抓取
## 环境搭建
![enter description here](https://gitee.com/margan/pictures/raw/master/小书匠/1580545532069.png)

![enter description here](https://gitee.com/margan/pictures/raw/master/小书匠/1580545532240.png)

![enter description here](https://gitee.com/margan/pictures/raw/master/小书匠/1580545532489.png)

![enter description here](https://gitee.com/margan/pictures/raw/master/小书匠/1580545532242.png)



### 安装fiddler

- 设置switchypmega后，没有办法联网．不设置抓不到数据

  

### genymotion的安装和填坑指南

- 下载死慢死慢　　７ｋ左右
- 已经解决（突然就好了）．．．可能网络问题，等等就好，神经病一样的网络．

- 附件提供下载包
```shell
# 安装virtualbox
sudo apt install virtualbox -y

cd ~/Downloads/
 chmod u+x ./genymotion-3.0.2-linux_x64.bin
 sudo ./genymotion-3.0.2-linux_x64.bin -d ~/opt
 
 #起别名

#让命令别名永久生效：

gedit ~/.bashrc

```
在~/bashrc文件中追加alias genymotion="~/opt/genymotion/genymotion" ,再source即可立即生效。
![enter description here](https://gitee.com/margan/pictures/raw/master/小书匠/1580545532455.png)
https://pan.baidu.com/s/1kUAftyR

#### 解决镜像下载慢的问题

- genymotion默认的路径是~/.Genymobile, 这个文件夹下有个日志文件genymotion.log。或者全局搜索cd / && sudo find ./ -name Genymobile，查看genymotion.log：vim - -    genymotion.log,
- 然后找到类似Starting download of http://dl.genymotion.com/dists/5.0.0/ova/genymotion_vbox86p_5.0_170928_172212.ova （在最后几行），把这个地址copy出来去浏览器下载。
- 下载后将.ova文件cp到~/.Genymotion/Genymotion/ova/下，重新操作gui配置镜像，发现可以直接用了．
#### 解决拖拽不能安装apk问题
- 下载附件genymotion-arm-translation_v1.1.zip
- 启动genymotion,在手机界面把这个zip文件拖到手机屏幕上,点确定安装,之后提示重启,重启之后，就不会有兼容性的问题了（安卓9.0版本不行，７．１之前的都可以，其他未作测试）
-
### 解决安装应用不兼容
- 下载附件Genymotion-ARM-Translation .zip
- 启动genymotion,在手机界面把这个zip文件拖到手机屏幕上,点确定安装,之后，重启即可
#### 想多了，填了很多坑了，ａｐｐ终于能安装了，但是打开过一会后出错
![enter description here](https://gitee.com/margan/pictures/raw/master/小书匠/1580545532238.png)
算了，我还是回ｗｉｎｄｏｗｓ下用夜神模拟器吧．（浪费６个小时搞这个鸡儿，以后还是多听过来人的建议吧）
## 安装mitmproxy

```shell
pip3.7 install mitmproxy
```

- 出错

  ```shell
  ......
  Collecting idna>=2.1
    Downloading idna-2.8-py2.py3-none-any.whl (58 kB)
       |████████████████████████████████| 58 kB 75 kB/s 
  Collecting pycparser
    Downloading pycparser-2.19.tar.gz (158 kB)
       |████████████████████████████████| 158 kB 72 kB/s 
  Collecting MarkupSafe>=0.23
    Downloading MarkupSafe-1.1.1-cp37-cp37m-manylinux1_x86_64.whl (27 kB)
  Installing collected packages: pyparsing, passlib, pycparser, cffi, zstandard, six, protobuf, click, tornado, Brotli, pyasn1, publicsuffix2, ldap3, hpack, hyperframe, h2, asn1crypto, idna, cryptography, pyOpenSSL, blinker, ruamel.yaml.clib, ruamel.yaml, urwid, pyperclip, h11, wsproto, Werkzeug, MarkupSafe, Jinja2, itsdangerous, flask, kaitaistruct, sortedcontainers, mitmproxy
  ERROR: Could not install packages due to an EnvironmentError: [Errno 13] Permission denied: '/home/dylan/.local/lib/python3.7/site-packages/pyparsing.py'
  Check the permissions.
  
  ```
- 解决方法

  ```shell
  sudo chown -R $USER  ~/.local
  ```

  ## mitmproxy使用

  ```shell
  mitmproxy
  
  mitmweb
  mitmdump -w test.txt
  
  ```
  
  ### chrom添加代理
  ![enter description here](https://gitee.com/margan/pictures/raw/master/小书匠/1580545532243.png)

#### How to install on Chrome on Debian/Ubuntu
- Using Chrome, hit a page on your server via HTTPS and continue past the red warning page (assuming you haven't done this already)
- Open up Chrome Settings > Show advanced settings > HTTPS/SSL > Manage Certificates
- Click the Authorities tab and scroll down to find your certificate under the Organization Name that you gave to the certificate
- Select it, click Edit (NOTE: in recent versions of Chrome, the button is now "Advanced" instead of "Edit"), check all the boxes and click OK. You may have to restart Chrome
  ![enter description here](https://gitee.com/margan/pictures/raw/master/小书匠/1580545532255.png)
 #### How to install on Ubuntu (Manually)
- Create a directory for extra CA certificates in /usr/share/ca-certificates:
$ sudo mkdir /usr/share/ca-certificates/extra
- Copy the CA mitmproxy.crt file to this directory:
$ sudo cp mitmproxy.crt /usr/share/ca-certificates/extra/mitmproxy.crt
- Let Ubuntu add the mitmproxy.crt file's path relative to /usr/share/ca-certificates to /etc/ca-certificates.conf:
$ sudo dpkg-reconfigure ca-certificates
- In case of a .pem file on Ubuntu, it must first be converted to a .crt file:
$ openssl x509 -in foo.pem -inform PEM -out foo.crt

### 安装packet capture
![enter description here](https://gitee.com/margan/pictures/raw/master/小书匠/1580545532477.png)
## appium 
- 下载地址
- http://appium.io/
![enter description here](https://gitee.com/margan/pictures/raw/master/小书匠/1580545532475.png)
![enter description here](https://gitee.com/margan/pictures/raw/master/小书匠/1580545532260.png)
![enter description here](https://gitee.com/margan/pictures/raw/master/小书匠/1580545532339.png)
![enter description here](https://gitee.com/margan/pictures/raw/master/小书匠/1580545532257.png)
## doker的安装
![enter description here](https://gitee.com/margan/pictures/raw/master/小书匠/1580548728337.png)

```shell
docker --version
docker run hello-world
docker run -it ubuntu bash
exit

#查看当前容器
docker pa -a
#查看当前镜像
docker image ls
docker rmi hello-world
docker rm ...
docker rmi hello-world
```
## 抓包工具的使用
### flidder的使用
![enter description here](https://gitee.com/margan/pictures/raw/master/小书匠/1580558149332.png)
![enter description here](https://gitee.com/margan/pictures/raw/master/小书匠/1580558371218.png)
### 模拟器安装证书，设置fiddler抓取移动端数据

- <font color="red">设置好代理后没网，需要关闭win10防火墙，重启fiddler。</font>
参考文档
https://blog.csdn.net/jss19940414/article/details/89875043
误导文档
https://www.jianshu.com/p/17abc8214281

### mitmproxy移动设备安装证书
和fiddler差不多，参考fiddler
- 使用
```shell
#清除
z
#返回
q
#过滤
f
# [f:!(~c 200)]，再次f,返回

i
#设置断点
a
#重新访问
```
- mitmproxy过滤数据
返回200状态的数据
![返回200状态的数据](https://gitee.com/margan/pictures/raw/master/小书匠/1580604360292.png)
返回baidu的数据
![enter description here](https://gitee.com/margan/pictures/raw/master/小书匠/1580604898107.png)

![enter description here](https://gitee.com/margan/pictures/raw/master/小书匠/1580605729976.png)
- mitmdump的使用
- ![enter description here](https://gitee.com/margan/pictures/raw/master/小书匠/1580606827022.png)
- test.py

- ![enter description here](https://gitee.com/margan/pictures/raw/master/小书匠/1580606865550.png)
- ![enter description here](https://gitee.com/margan/pictures/raw/master/小书匠/1580607971659.png)
## app数据抓取
## 移动端自动化测试工具appium的使用
### 安装jdk
- https://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html
- 由于uiautomator需要jdk8，所以不要安装高版本。
- 根目录新建文件夹
![enter description here](https://gitee.com/margan/pictures/raw/master/小书匠/1580858132330.png)
- 配置环境变量

变量名：Path
变量值：C:\ProgramData\Oracle\Java\javapath;%java_home%\bin;%java_home%\jre\bin
新建
变量名：JAVA_HOME
变量值：C:\java\jdk
新建
变量名：ClassPath
变量值：.;%JAVA_HOME%\lib\dt.jar;%JAVA_HOME%\lib\tools.jar;
![enter description here](https://gitee.com/margan/pictures/raw/master/小书匠/1580858094475.png)
- 打开cmd输入java测试是否安装成功
### 安装sdk
![enter description here](https://gitee.com/margan/pictures/raw/master/小书匠/1580865163003.png)
- http://tools.android-studio.org/index.php/sdk
- 配置环境变量
- 
ANDROID_HOME	C:\SDK
PATH	;%ANDROID_HOME%\platform-tools;%ANDROID_HOME%\tools;
- 打开sdk manager.exe，安装必要的包，如果安装速度很慢注意更换安装源
### adb工具的使用
![enter description here](https://gitee.com/margan/pictures/raw/master/小书匠/1580869308561.png)
- 使用adb命令工具，需要移动客户端（手机）开启开发者模式，并允许USB调试

<font color="red">注意，adb工具环境变量中最好不要起别名，很可能无法被系统识别</font>
```shell
adb start-server
adb adb version
adb devices

netstat -ano}findstr "5037"
taskkill -pid pid_num -f
```
## 微信公众号的爬取

- 手机端用fildder和charles无法获取数据（猜测可能是微信使用了自己的验证规则）
- 抓取最佳方案为通过个人公众号发布超链接文章获取其公众号文章接口
![enter description here](https://gitee.com/margan/pictures/raw/master/小书匠/1580964299090.png)
- 经过ssl pinning的突破后，可以爬取到数据，但是公众号的历史数据只能爬到图片，点击进入文章中，只能爬取到评论和两个js文件。。。
![enter description here](https://gitee.com/margan/pictures/raw/master/小书匠/1581061281925.png)
![enter description here](https://gitee.com/margan/pictures/raw/master/小书匠/1581061308069.png)
-:cry::cry::cry:
## fiddler抓不到包分析
https://blog.csdn.net/wangjun5159/article/details/52202059
## 百家号爬取
- charles redmi k20pro无法安装charles的证书：提示无法打开改文件，因为文件内容无法读取。。。
- 百度app不信任fiddler的证书，安装证书后会提示。。。
- 使用fiddlercertmaker.exe后，手机真机可以正常访问，但是抓不到包。
![enter description here](https://gitee.com/margan/pictures/raw/master/小书匠/1581074677588.png)
- 可以返回主页html，但是加载不出来界面
- 手机真机
![enter description here](https://gitee.com/margan/pictures/raw/master/小书匠/1581074988265.png)
- 接下来是xposed just trust me后
## 夜神android版本的选取
- 4.4.2 无法启动微信，会闪退
- 7.几的百度app运行不稳定，会停止运行
- 5.几能正常运行两款app，但是xposed后，运行一会出现停止运行（考虑可能是xposed影响或者内存不够，配置低。）

