---
title: Go语言深度学习笔记----慕课ccmouse
date: 2017-12-15
lastmod: 2017-12-15
author: Bob Dylan
cover: /img/default3.jpg
categories: ["golang"]
tags: [ "golang"]
# showcase: true
draft: true
grammar_cjkRuby: true
---
[TOC]

## 第一章 课程介绍和环境安装

### 第一节 课程介绍
![Alt text](https://gitee.com/margan/pictures/raw/master/小书匠/1584427766899.png)
![Alt text](https://gitee.com/margan/pictures/raw/master/小书匠/1584427766897.png)
![Alt text](https://gitee.com/margan/pictures/raw/master/小书匠/1584427766955.png)
![Alt text](https://gitee.com/margan/pictures/raw/master/小书匠/1584427766963.png)

### 第二节 环境安装
- [ ] vscode golang环境配置
https://www.cnblogs.com/nickchou/p/9038114.html
其中lint和dlv插件安装会失败
lint需要将src/github.com/golang/lint复制到 src/golang.org/x/
dlv需要到http://github.com/derekparker/delve   clone下来
git clone git@github.com:derekparker/delve.git到src/golang.org/x/中，然后再将delv文件夹复制到src/github.com/derekparker/下，然后执行
go install github.com/derekparker/delv/cmd/dlv


## 第二章 Go语法
### 第一节 变量
![Alt text](https://gitee.com/margan/pictures/raw/master/小书匠/1584427766926.png)
![Alt text](https://gitee.com/margan/pictures/raw/master/小书匠/1584427766895.png)
![Alt text](https://gitee.com/margan/pictures/raw/master/小书匠/1584427766957.png)
![Alt text](https://gitee.com/margan/pictures/raw/master/小书匠/1584427766985.png)
![Alt text](https://gitee.com/margan/pictures/raw/master/小书匠/1584427766961.png)
![Alt text](https://gitee.com/margan/pictures/raw/master/小书匠/1584427766967.png)
![Alt text](https://gitee.com/margan/pictures/raw/master/小书匠/1584427766965.png)
### 第二节 常量
![Alt text](https://gitee.com/margan/pictures/raw/master/小书匠/1584427766969.png)
![Alt text](https://gitee.com/margan/pictures/raw/master/小书匠/1584427766974.png)
![Alt text](https://gitee.com/margan/pictures/raw/master/小书匠/1584427766971.png)
### 第三节 循环语句
#### if语句
![Alt text](https://gitee.com/margan/pictures/raw/master/小书匠/1584427766979.png)
#### switch语句
![Alt text](https://gitee.com/margan/pictures/raw/master/小书匠/1584427766977.png)
#### for语句
![Alt text](https://gitee.com/margan/pictures/raw/master/小书匠/1584427766983.png)
![Alt text](https://gitee.com/margan/pictures/raw/master/小书匠/1584427766980.png)
![Alt text](https://gitee.com/margan/pictures/raw/master/小书匠/1584427766987.png)
![Alt text](https://gitee.com/margan/pictures/raw/master/小书匠/1584427767970.png)
### 第四节函数
![Alt text](https://gitee.com/margan/pictures/raw/master/小书匠/1584427767984.png)
![Alt text](https://gitee.com/margan/pictures/raw/master/小书匠/1584427767979.png)
![Alt text](https://gitee.com/margan/pictures/raw/master/小书匠/1584427767973.png)

### 第五节 指针
![Alt text](https://gitee.com/margan/pictures/raw/master/小书匠/1584427701689.png)
![Alt text](https://gitee.com/margan/pictures/raw/master/小书匠/1584427701713.png)
![Alt text](https://gitee.com/margan/pictures/raw/master/小书匠/1584427768276.png)
值交换
![Alt text](https://gitee.com/margan/pictures/raw/master/小书匠/1584427701551.png)
![Alt text](https://gitee.com/margan/pictures/raw/master/小书匠/1584427701937.png)
### 第六节 数组
![Alt text](https://gitee.com/margan/pictures/raw/master/小书匠/1584427768274.png)
![Alt text](https://gitee.com/margan/pictures/raw/master/小书匠/1584427768279.png)
![Alt text](https://gitee.com/margan/pictures/raw/master/小书匠/1584427768283.png)
![Alt text](https://gitee.com/margan/pictures/raw/master/小书匠/1584427768405.png)
![Alt text](https://gitee.com/margan/pictures/raw/master/小书匠/1584427768280.png)
### 第七节 切片
![Alt text](https://gitee.com/margan/pictures/raw/master/小书匠/1584427768404.png)
![Alt text](https://gitee.com/margan/pictures/raw/master/小书匠/1584427768281.png)
![Alt text](https://gitee.com/margan/pictures/raw/master/小书匠/1584427768282.png)
![Alt text](https://gitee.com/margan/pictures/raw/master/小书匠/1584427768411.png)
### 第八节 Map
![Alt text](https://gitee.com/margan/pictures/raw/master/小书匠/1584427766395.png)
![Alt text](https://gitee.com/margan/pictures/raw/master/小书匠/1584427766989.png)
![Alt text](https://gitee.com/margan/pictures/raw/master/小书匠/1584427767363.png)
![Alt text](https://gitee.com/margan/pictures/raw/master/小书匠/1584427767386.png)

### 第九节 字符串
![Alt text](https://gitee.com/margan/pictures/raw/master/小书匠/1584427767391.png)


##第三章 面向对象
### 第一节 结构体和方法
![Alt text](https://gitee.com/margan/pictures/raw/master/小书匠/1584427767460.png)
![Alt text](https://gitee.com/margan/pictures/raw/master/小书匠/1584427767659.png)
![Alt text](https://gitee.com/margan/pictures/raw/master/小书匠/1584427767667.png)
![Alt text](https://gitee.com/margan/pictures/raw/master/小书匠/1584427767641.png)
![Alt text](https://gitee.com/margan/pictures/raw/master/小书匠/1584427767681.png)
![Alt text](https://gitee.com/margan/pictures/raw/master/小书匠/1584427701590.png)
![Alt text](https://gitee.com/margan/pictures/raw/master/小书匠/1584427767642.png)
### 第二节 包
![Alt text](https://gitee.com/margan/pictures/raw/master/小书匠/1584427767645.png)
![Alt text](https://gitee.com/margan/pictures/raw/master/小书匠/1584427767647.png)
![Alt text](https://gitee.com/margan/pictures/raw/master/小书匠/1584427767649.png)
### 第三节 GOPATH环境变量
### 第四章 接口
### 第一节 duck typeing
![Alt text](https://gitee.com/margan/pictures/raw/master/小书匠/1584427767651.png)
![Alt text](https://gitee.com/margan/pictures/raw/master/小书匠/1584427767654.png)
![Alt text](https://gitee.com/margan/pictures/raw/master/小书匠/1584427767968.png)
![Alt text](https://gitee.com/margan/pictures/raw/master/小书匠/1584427767964.png)
### 第二节 接口的值类型
![Alt text](https://gitee.com/margan/pictures/raw/master/小书匠/1584427767981.png)
![Alt text](https://gitee.com/margan/pictures/raw/master/小书匠/1584427767982.png)
### 第三节接口的组合
![Alt text](https://gitee.com/margan/pictures/raw/master/小书匠/1584427701938.png)
![Alt text](https://gitee.com/margan/pictures/raw/master/小书匠/1584427768039.png)

