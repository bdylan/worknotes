---
title: Ubuntu安装及Python环境的配置
date: 2019-01-17
lastmod: 2019-01-17
author: Bob Dylan
cover: /img/default2.jpg
categories: ["python"]
tags: ["ubuntu", "python"]
# showcase: true
draft: true
grammar_cjkRuby: true
---
[toc]
# Ubuntu安装及Python环境的配置
## 1. ubuntu18.04基于win10双系统的安装
### 1.1 分区
1.  查看系统BIOS模式
win+R, 输入msinfo32, 得到系统信息,这里我的BIOS模式是传统模式（MBR），所以这里以MBR为例
![enter description here](https://gitee.com/margan/pictures/raw/master/小书匠/1579588104632.png)
2. 下载ubuntu系统
链接： https://www.ubuntu.com/download/desktop
这里LTS表示长期维护版本，也就2年。下面那个是最新版，根据需求喜好随便下载一个，这里我以ubuntu18.04 LTS为例
![enter description here](https://gitee.com/margan/pictures/raw/master/小书匠/1579588104610.png)
3. 分出空闲空间
快捷键“win+X”->“磁盘管理”->“压缩卷”（Windows安装在固态硬盘则压缩固态硬盘，机械硬盘同理），压缩完之后会多出了一块“未分配空间”。我的固态是250G，我选择分108G（110592M）给Ubuntu，这里根据需要分就行，磁盘小分40g也够用。下图是我安装好双系统后被分出去的空间使用情况，刚分完108g是一块黑的
![enter description here](https://gitee.com/margan/pictures/raw/master/小书匠/1579588104458.png)

### 1.2 设置u盘启动
1. 制作U盘启动盘
下载链接：https://rufus.ie/zh_CN.html
这里使用的是Rufus，选好下载的镜像，分区方案选择第一个（因为是装MBR的，UEFI就选第二个）其他默认就行
![enter description here](https://gitee.com/margan/pictures/raw/master/小书匠/1579588104454.png)
2. 重启电脑
让自己的电脑从U盘启动，如果你的电脑是mbr格式，就不要以UEFI的格式进入，如果你的电脑是GPT格式，则必须以UEFI格式进入。我的是联想电脑，关机状态下，按开关键旁边的恢复按键即可进入BIOS菜单
### 1.3 安装ubuntu
1. install ubuntu
进入Boot menu选择界面，选择“Istall Ubuntu”，图形化的安装界面，软件暂时不要安装，继续，当出现要手动分盘符的时候，请选择“其它选项
 ![enter description here](https://gitee.com/margan/pictures/raw/master/小书匠/1579588104577.png)
 2. 分区
 给108G空间进行分区操作，分配4个区间，分区时候选中空闲空间点下面加号
<font color="#dd0000">分区的时候如果碰到分区一块后无法再分，那是因为系统只能识别4块主分区，windows已经占了3个，可以先分其他区，再分主分区,一下是我的分区示例 </font>

|   目录  |  建议大小   | 格式    |描述|
| --- | --- | --- |---|
|   /  |  50G   |  ext4   | 根目录  |
|   swap  |  物理内存的两倍   | swap    | 交换空间：交换分区相当于Windows中的“虚拟内存”，如果内存低的话（1-4G），物理内存的两倍，高点的话（8-16G）要么等于物理内存，要么物理内存+2g左右，|
| /boot    |  200M   |  ext4    | 空间起始位置 分区格式为ext4 /boot 建议：应该大于400MB或1GB Linux的内核及引导系统程序所需要的文件，比如 vmlinuz initrd.img文件都位于这个目录中。在一般情况下，GRUB或LILO系统引导管理器也位于这个目录；启动撞在文件存放位置，如kernels，initrd，grub。|
|   /home  |   50G  |   ext4   |用户工作目录；个人配置文件，如个人环境变量等；所有账号分配一个工作目录。|
<font color="#dd0000">第四次分区后请不要急着点“安装” 分区设置完毕后，下方还有一项“安装启动引导器的设备”选择/boot所在的盘符。接着安装，自动重新启动Win10。 </font>
- 如果重启失败，强行按电源键重启进入windows系统
### 1.4 设置开机引导
1. 下载easyBCD 
链接：https://pc.qq.com/detail/11/detail_161271.html 
打开软件，设置开机引导，添加条目，如果您是传统BIOS和MBR格式参照下图
![enter description here](https://gitee.com/margan/pictures/raw/master/小书匠/1579588104455.png)
这里可以选择默认开机系统
![enter description here](https://gitee.com/margan/pictures/raw/master/小书匠/1579588104457.png)
2. 重启，完成

本节参考文档：https://blog.csdn.net/shaoyezuizuishaui/article/details/90647385
https://www.cnblogs.com/masbay/p/11627727.html

---
 ## 2. ubuntu安装后的配置
### 2.1 设置阿里云的源
按super+a，搜索softeware & update点开设置成下图所示即可
![enter description here](https://gitee.com/margan/pictures/raw/master/小书匠/1579591343290.png)
![enter description here](https://gitee.com/margan/pictures/raw/master/小书匠/1579591125848.png)
### 2.2 安装谷歌拼音输入法
我之前安装了搜狗拼音，一直出现乱码情况，找了很多办法也没有解决，干脆撞了个谷歌拼音（如果有小伙伴也有这样的问题，可以给我留言，我们一起讨论，或者有大佬解决了这个问题可以告诉小弟）
```shell
sudo apt install fcitx-googlepinyin
```
1.  去Ubuntu系统设置->区域和语言->管理已安装的语言,进去和可能提示你安装一些东西，点安装就行了，安装完成后出现语言支持框，选择汉语　然后点应用到整个系统，在键盘输入法系统选择fcitx，没有的话去应用商店搜索安装下，一般都预装了的选择后，重启系统
![enter description here](https://gitee.com/margan/pictures/raw/master/小书匠/1579591711467.png)
-2. 重启成功后，右上角状态栏有个输入法标识，点击，出现配置，然后选择谷歌拼音输入法就成
或者输入下列命令
```shell
fcitx-config-gtk3
```
- 如果输入法候选栏透明，去第三步的窗口，选择附加组建->高级->去掉经典皮肤勾选**
### 2.3 安装ssr
https://github.com/qingshuisiyuan/electron-ssr-backup/releases
下载后直接鼠标双击即可安装
或者执行
```shell
dpkg -i electron-ssr
```
#### 2.3.1 设置开机自启动
1. 查看软件安装路径
```shell

whereis electron-ssr  # 查看软件安装路径

ls -al /usr/local/bin/electron-ssr  #查看软链接指向哪里

```
![enter description here](https://gitee.com/margan/pictures/raw/master/小书匠/1579607340100.png)

2. super+a搜索startup  appllications preferences添加可执行程序，如图
![enter description here](https://gitee.com/margan/pictures/raw/master/小书匠/1579607642083.png)


### 2.4 typora或者小书匠的安装
#### 2.4.1 typora的安装
```shell

sudo apt-get update

sudo apt-get install indicator-sysmonitor

indicator-sysmonitor &
```
[官方教程](https://support.typora.io/Typora-on-Linux/)
#### 2.4.2 小书匠的安装
https://github.com/suziwen/markdownxiaoshujiang/releases/tag/v7.9.0
下载zip格式的即可，解压
```shell
unzip Story-writer
```
##### 2.4.2.1 创建桌面图标
1. 创建桌面图标文件
在/usr/share/applications 目录下，新建story-writer.desktop,打开这个文件
```shell
vim /usr/share/applications/eclipse.desktop
```
2. 编辑为：
```shell?linenums
Comment=IDEA
Exec=/usr/local/Story-writer/Story-writer #根据软件的具体执行路径修改
Terminal=false   #软件打开时是否启动终端
Type=Application
Icon=/usr/local/Story-writer/Story-writer.png #根据软件的具体执行路径修改
Categories=Development;

```
3. 进入/usr/share/applications 将相应图标右键复制到桌面即可
---
推荐：小书匠（这里是给喜欢写博客的朋友推荐的）
理由：可以方便的实现图片转链接，我配置了gitee图床，笔记可以同步到印象笔记
之前我安装picgo，无法运行，不知道你们有没有这个问题，所以我就放弃了typora+picgo
如果有更好的方法，欢迎大家和我交流

---

### 2.3 gitee图床的搭建、小书匠绑定图床
1. 创建仓库，网上教程很多，这里不再赘述。[直通车](]https://gitee.com/)
2. 生成私人令牌：https://gitee.com/profile/personal_access_tokens/new
3. 绑定小书匠
![enter description here](https://gitee.com/margan/pictures/raw/master/小书匠/1579594755864.png)
4. 绑定成功或有提示，成功后，系统剪贴板的图片会自动生成网络图片，直接粘贴即可用，这样如果要发布博客，直接复制md文档内容过去就可以，不用考虑图片丢失的问题。

### 2.4 安装谷歌浏览器
https://www.google.cn/intl/zh-CN/chrome/
参考安装ssr
### 2.5 配置截图工具
打开系统设置找到devices将系统自带的截图工具设置自己常用的快捷键就行
![enter description here](https://gitee.com/margan/pictures/raw/master/小书匠/1579595317107.png)
### 2.6 安装美化工具
tweak和dash安装，参考后盾人文档即可

本节参考文档：
https://blog.csdn.net/linfeng886/article/details/83958806
后盾人：http://houdunren.gitee.io/note/linux/10%20ubuntu.html#%E5%AE%89%E8%A3%85%E7%B3%BB%E7%BB%9F

---

## 3. python环境的搭建
### 3.1 安装python3.7和pip3
1. 安装依赖
```shell
sudo apt-get install build-essential checkinstall
sudo apt-get install libreadline-gplv2-dev libncursesw5-dev libssl-dev libsqlite3-dev tk-dev libgdbm-dev libc6-dev libbz2-dev

# #
sudo apt-get install zlib1g-dev libbz2-dev libssl-dev libncurses5-dev libsqlite3-dev libreadline-dev tk-dev libgdbm-dev libdb-dev libpcap-dev xz-utils libexpat1-dev liblzma-dev libffi-dev libc6-dev
```
2. 设置安装路径
我一般安装在/usr/local/下面
```shell
sudo mkdir -p /usr/local/python3
```
3. 下载python
```shell
# 下载到指定位置
cd /home/download
sudo wget https://www.python.org/ftp/python/3.7.0/Python-3.7.0.tgz

sudo tar xzf Python-3.7.0.tgz
```
4. 编译并安装（make的时候时间要久一点）
执行这步是后面最好加上 --enable-optimizations 会自动安装pip3及优化配置
```shell
cd Python-3.7.0
sudo ./configure --prefix=/usr/local/python3 --enable-optimizations
sudo make
sudo make install
```
5.配置软链接
我的系统是默认安装了python3.6，我想要安装3.7版本
千万不要随便卸载原来的python版本，不管是2.6还是3.6。不信邪的可以试试
不要随便改原来的链接 ，很多软件（比如terminal的快捷方式依赖python命令，没有的话无法用。ssr依赖python3.6,直接改它的软链接，会导致ssr失效） 


```shell
python3 -V
pip3 -V

#添加python3的软链接
ln -s /usr/local/python3/bin/python3.7 /usr/bin/python3.7
#添加 pip3 的软链接
ln -s /usr/local/python3/bin/pip3.7 /usr/bin/pip3.7

# 查看一下是否成功
python3.7 -V
pip3.7 -V
```
参考文档：https://juejin.im/post/5d5e3891f265da03bf0f48cc
注意：如果

### 3.2 安装pipenv
1. 安装pipenv
pip install --user pipenv
这安装在了用户的$HOME/.local/bin目录下，需要自行将该目录添加到PATH中，打开.bashrc，添加

```shell
export PATH="家目录/.local/bin:$PATH"
```
pipenv的更新
```shell
pip install --user --upgrade pipenv
```
2. 使用pipenv
初始化虚拟环境blo
```shell
pipenv --three # python3
pipenv --two # python2
```
这会在当前目录下创建一个Pipfile文件，自动记录所有安装的和卸载的模块

3. 进入虚拟环境，
```shell
pipenv shell
```
注意，直接使用这个命令可以自动初始化虚拟环境

4. 退出虚拟环境
```shell
exit
```
本节参考文档：https://zhuanlan.zhihu.com/p/51444311
### 3.3 安装pycharm
打开系统的软件哭直接搜索pycharm安装即可
![enter description here](https://gitee.com/margan/pictures/raw/master/小书匠/1579597700621.png)


:smile::smile::smile::smile:

2020年01月21日 17时09分56秒
