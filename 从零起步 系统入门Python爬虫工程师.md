---
title: 从零起步 系统入门Python爬虫工程师
date: 2017-12-17
lastmod: 2017-12-17
author: Bob Dylan
cover: /img/default4.jpg
categories: ["python"]
tags: ["python", "爬虫"]
# showcase: true
draft: true
grammar_cjkRuby: true
---


# 第一章 环境搭建 网络爬虫了解

## 快捷键
![enter description here](https://gitee.com/margan/pictures/raw/master/小书匠/1584426732984.png)
## 作用
![enter description here](https://gitee.com/margan/pictures/raw/master/小书匠/1584426732990.png)
# 第二章计算机网络协议基础
## 
![enter description here](https://gitee.com/margan/pictures/raw/master/小书匠/1584426733184.png)
## 推荐书本
### ==**TCP/IP协议族** #F44336==
### ==**计算机网络（第六版）** #F44336==
![enter description here](https://gitee.com/margan/pictures/raw/master/小书匠/1584426733535.png)
![enter description here](https://gitee.com/margan/pictures/raw/master/小书匠/1584426733539.png)
![enter description here](https://gitee.com/margan/pictures/raw/master/小书匠/1584426733655.png)
![enter description here](https://gitee.com/margan/pictures/raw/master/小书匠/1584426733377.png)
![enter description here](https://gitee.com/margan/pictures/raw/master/小书匠/1584426734054.png)
![enter description here](https://gitee.com/margan/pictures/raw/master/小书匠/1584426733745.png)
![enter description here](https://gitee.com/margan/pictures/raw/master/小书匠/1584426734056.png)
![enter description here](https://gitee.com/margan/pictures/raw/master/小书匠/1584426733767.png)
![enter description here](https://gitee.com/margan/pictures/raw/master/小书匠/1584426733923.png)

---
<div style="font-family:Arial,Microsoft YaHei,黑体,宋体,sans-serif; font-size:18px; color:; ">
&nbsp;<i class="fas fa-exclamation"></i>

  * ConnectionRefusedError: [WinError 10061] 由于目标计算机积极拒绝，无法连接
 
<i class="fas fa-coffee "></i>

* 不要急着找防火墙，redis，等乱七八糟的问题
* 出现错误百度说可能是redis-server没有开启，安装redis时发现防火墙没有开启，打开services.msc时发现所有安全相关的服务都无法开启，然后改注册表，更新系统，修复系统，360急救箱拯救系统，杀毒，试了一遍都没用。搞了两个小时啥用没有。睡觉时突然想到可能360管理的启动项关了，第二天起来搞了一下发现系统的安全服务都能用了,但是服务还是全部为灰色，不管了，能用就行。修好系统，装好redis，返回pycharm后，运行client确实没问题了，多疑的我把redis关了又试了一遍，发现还不报错。细看了一下，发现出错是因为要先运行server端。一切回到了最初。废了近四个小时，只因为一点粗心，留念谨记！先思考，在百度！

<i class="fas fa-question "></i>
* python基本语法

</div>
---
# 第三章 网络编程
![enter description here](https://gitee.com/margan/pictures/raw/master/小书匠/1584426733379.png)
![enter description here](https://gitee.com/margan/pictures/raw/master/小书匠/1584426734058.png)
![enter description here](https://gitee.com/margan/pictures/raw/master/小书匠/1584426733410.png)
![enter description here](https://gitee.com/margan/pictures/raw/master/小书匠/1584426733925.png)
![enter description here](https://gitee.com/margan/pictures/raw/master/小书匠/1584426733511.png)
![enter description here](https://gitee.com/margan/pictures/raw/master/小书匠/1584426733928.png)
![enter description here](https://gitee.com/margan/pictures/raw/master/小书匠/1584426734025.png)
![enter description here](https://gitee.com/margan/pictures/raw/master/小书匠/1584426734103.png)
![enter description here](https://gitee.com/margan/pictures/raw/master/小书匠/1584426734759.png)
![enter description here](https://gitee.com/margan/pictures/raw/master/小书匠/1584426734760.png)
![enter description here](https://gitee.com/margan/pictures/raw/master/小书匠/1584426734138.png)

# 第四章 爬虫初体验
![enter description here](https://gitee.com/margan/pictures/raw/master/小书匠/1584426734665.png)
![enter description here](https://gitee.com/margan/pictures/raw/master/小书匠/1584426735396.png)


# 多线程和线程池操作
![enter description here](https://gitee.com/margan/pictures/raw/master/小书匠/1584426734755.png)
![enter description here](https://gitee.com/margan/pictures/raw/master/小书匠/1584426734758.png)
![enter description here](https://gitee.com/margan/pictures/raw/master/小书匠/1584426734912.png)
![enter description here](https://gitee.com/margan/pictures/raw/master/小书匠/1584426734568.png)
![enter description here](https://gitee.com/margan/pictures/raw/master/小书匠/1584426734812.png)
![enter description here](https://gitee.com/margan/pictures/raw/master/小书匠/1584426734610.png)

# 实现模拟登陆和验证码
![enter description here](https://gitee.com/margan/pictures/raw/master/小书匠/1584426734649.png)
![enter description here](https://gitee.com/margan/pictures/raw/master/小书匠/1584426734909.png)

# 爬虫和反爬虫
![enter description here](https://gitee.com/margan/pictures/raw/master/小书匠/1584426734911.png)
![enter description here](https://gitee.com/margan/pictures/raw/master/小书匠/1584426734757.png)
![enter description here](https://gitee.com/margan/pictures/raw/master/小书匠/1584426734761.png)
![enter description here](https://gitee.com/margan/pictures/raw/master/小书匠/1584426734852.png)
![enter description here](https://gitee.com/margan/pictures/raw/master/小书匠/1584426734866.png)
![enter description here](https://gitee.com/margan/pictures/raw/master/小书匠/1584426735032.png)
# 如何成为高级的爬虫工程师
![enter description here](https://gitee.com/margan/pictures/raw/master/小书匠/1584426734925.png)
![enter description here](https://gitee.com/margan/pictures/raw/master/小书匠/1584426735194.png)
![enter description here](https://gitee.com/margan/pictures/raw/master/小书匠/1584426735181.png)
![enter description here](https://gitee.com/margan/pictures/raw/master/小书匠/1584426735193.png)
![enter description here](https://gitee.com/margan/pictures/raw/master/小书匠/1584426735195.png)
![enter description here](https://gitee.com/margan/pictures/raw/master/小书匠/1584426735391.png)
![enter description here](https://gitee.com/margan/pictures/raw/master/小书匠/1584426735392.png)