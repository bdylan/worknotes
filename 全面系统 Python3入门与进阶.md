---
title: 全面系统 Python3入门与进阶
date: 2019-10-17
lastmod: 2019-10-17
author: Bob Dylan
cover: /img/default3.jpg
categories: ["python"]
tags: [ "python"]
# showcase: true
draft: true
grammar_cjkRuby: true
---

# 第一章 
## 基本类型和注意
* number(int float  bool  )complex str tuple list set dict
* type(2//2)  int
* type(2/2)  float

* 数字交换
* ‘’  ， “” ， ‘’‘  ’‘’， “”“  ”“”区别
* bool(0),bool([]),bool({}),bool('') =flase
* 'let\'s go'  转义字符
* 
![enter description here](https://gitee.com/margan/pictures/raw/master/小书匠/1584426498226.png)
![enter description here](https://gitee.com/margan/pictures/raw/master/小书匠/1584426498229.png)
![enter description here](https://gitee.com/margan/pictures/raw/master/小书匠/1584426498247.png)
![enter description here](https://gitee.com/margan/pictures/raw/master/小书匠/1584426498249.png)

## 组的概念（序列）--str list tuple
* 表示组的方式有很多种
* [1,2,3,4,5,6]--列表
* 嵌套列表（二维数组）
* 元组和列表的区别()  []
*![enter description here](https://gitee.com/margan/pictures/raw/master/小书匠/1584426498252.png)
* 序列共有的操作 
* 切片
*![enter description here](https://gitee.com/margan/pictures/raw/master/小书匠/1584426498254.png)
* ord('d') ascii码转换
* 集合特点
 * 无序
 * 去重复

 * dict 有序，有很多key，value，{}表示，集合类型（set）。
 * dict中不能有重复的，不出错，但自动去掉了
 * dict中key可以是number、str。。。不可变类型
 * 集合：是无序的，不支持下标索引，它是可变的数据类型，集合中的元素是唯一的，一般用于元组或者列表中的元素去重
 https://www.cnblogs.com/huangqihui/p/9270759.html
 # 变量、运算符
 * 命名  ；1 字母 数字 下划线； 2关键字不能用
 * int str tuple （值类型），list set dict（引用类型）
  ![enter description here](https://gitee.com/margan/pictures/raw/master/小书匠/1584426498058.png)
  * id()  内存地址
  * 加减运算符优先级小于比较运算符
  * not返回true和false
  * 成员运算符 in  not in
  * 身份运算符 is not is 
  *![enter description here](https://gitee.com/margan/pictures/raw/master/小书匠/1584426498096.png)
  * isinstance(a,str)判断类型函数
  * 对象的三个特征  id value type--is ，==， isinstance（不建议用type判断）
  # 分支，循环，条件，枚举
  * not>and>or优先级
  ![enter description here](https://gitee.com/margan/pictures/raw/master/小书匠/1584426498257.png)
  * vscode插件 
  * pylint格式规范，  sinppet（tab键值跳转）
  ![enter description here](https://gitee.com/margan/pictures/raw/master/小书匠/1584426498119.png)
  * break continue
  ```python
  for i in range(0,10):
      print(x)
	 
```
![enter description here](https://gitee.com/margan/pictures/raw/master/小书匠/1584426498141.png)
	* 会写代码，非常容易
	* 高性能、封装性（可复用）、抽象
	# 包，模块，函数，变量作用域
	![enter description here](https://gitee.com/margan/pictures/raw/master/小书匠/1584426498163.png)
	* __init__.py   批量导入包，控制模块输出
	* 包和模块不会被重复导入，
	# 面向对象
	![enter description here](https://gitee.com/margan/pictures/raw/master/小书匠/1584426498198.png)
	![enter description here](https://gitee.com/margan/pictures/raw/master/小书匠/1584426498266.png)
	* 构造函数
	* 构造函数自动运行，实例化的时候就调用了，不能随便返回值，只能返回 None
	* 构造函数让模板生成不同的对象，初始化对象的属性
	![enter description here](https://gitee.com/margan/pictures/raw/master/小书匠/1584426498275.png)
	 # 正则表达式和JSON
	 ![enter description here](https://gitee.com/margan/pictures/raw/master/小书匠/1584426498210.png)
	 ![enter description here](https://gitee.com/margan/pictures/raw/master/小书匠/1584426498285.png)
	 * ？贪婪非贪婪、字符串取0或者1次
	 ![enter description here](https://gitee.com/margan/pictures/raw/master/小书匠/1584426498212.png)
![enter description here](https://gitee.com/margan/pictures/raw/master/小书匠/1584426498224.png)
![enter description here](https://gitee.com/margan/pictures/raw/master/小书匠/1584426498287.png)
![enter description here](https://gitee.com/margan/pictures/raw/master/小书匠/1584426498524.png)
![enter description here](https://gitee.com/margan/pictures/raw/master/小书匠/1584426498552.png)
![enter description here](https://gitee.com/margan/pictures/raw/master/小书匠/1584426498392.png)
![enter description here](https://gitee.com/margan/pictures/raw/master/小书匠/1584426498566.png)
![enter description here](https://gitee.com/margan/pictures/raw/master/小书匠/1584426498580.png)
![enter description here](https://gitee.com/margan/pictures/raw/master/小书匠/1584426498409.png)

---
* 大小写快速切换，页面切换、快速删整行
---
![enter description here](https://gitee.com/margan/pictures/raw/master/小书匠/1584426498611.png)

# python高级语法和用法
* 枚举类型enum
* 函数
 ![enter description here](https://gitee.com/margan/pictures/raw/master/小书匠/1584426498431.png)
 * 函数 = 函数 +  环境变量
 * 现场
 ![enter description here](https://gitee.com/margan/pictures/raw/master/小书匠/1584426498612.png)
 # 函数式编程：匿名函数、高阶函数、装饰器
 * lanmbda表达式,三元表达式
 ![enter description here](https://gitee.com/margan/pictures/raw/master/小书匠/1584426498433.png)
 ![enter description here](https://gitee.com/margan/pictures/raw/master/小书匠/1584426498434.png)
 ![enter description here](https://gitee.com/margan/pictures/raw/master/小书匠/1584426498653.png)
 ![enter description here](https://gitee.com/margan/pictures/raw/master/小书匠/1584426498654.png)
 ![enter description here](https://gitee.com/margan/pictures/raw/master/小书匠/1584426498655.png)
 ![enter description here](https://gitee.com/margan/pictures/raw/master/小书匠/1584426498656.png)
 ![enter description here](https://gitee.com/margan/pictures/raw/master/小书匠/1584426498657.png)
 ![enter description here](https://gitee.com/margan/pictures/raw/master/小书匠/1584426498819.png)
 ![enter description here](https://gitee.com/margan/pictures/raw/master/小书匠/1584426498770.png)
 # 杂记录
 * 字典代替switch...case
 ![enter description here](https://gitee.com/margan/pictures/raw/master/小书匠/1584426498802.png)
 ![enter description here](https://gitee.com/margan/pictures/raw/master/小书匠/1584426498818.png)
 ![  ](https://gitee.com/margan/pictures/raw/master/小书匠/1584426498828.png)
 ![enter description here](https://gitee.com/margan/pictures/raw/master/小书匠/1584426498829.png)
 ![enter description here](https://gitee.com/margan/pictures/raw/master/小书匠/1584426498830.png)